﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailingEndpoint.Monitor.Messages.Events
{
    public interface IAmAnAlertableError
    {
        string Id { get; set; }
        string Message { get; set; }
    }
}
