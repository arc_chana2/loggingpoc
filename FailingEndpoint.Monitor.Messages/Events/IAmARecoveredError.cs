﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailingEndpoint.Monitor.Messages.Events
{
    public interface IAmARecoveredError
    {
        string Id { get; set; }
    }
}
