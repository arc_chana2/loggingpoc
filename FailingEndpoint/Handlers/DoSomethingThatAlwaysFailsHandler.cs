﻿using System;
using System.Threading.Tasks;
using FailingEndpoint.Messages.Commands;
using NServiceBus;
using FailingEndpoint.Monitor.Messages.Events;

namespace FailingEndpoint.Handlers
{
    public sealed class DoSomethingThatAlwaysFailsHandler : IHandleMessages<DoSomethingThatAlwaysFails>
    {
        public async Task Handle(DoSomethingThatAlwaysFails message, IMessageHandlerContext context)
        {
            await Task.Run(() => Console.WriteLine($"Doing Something: {message.Id}"));
            throw new Exception("I'm failing");
            //IAmAnAlertableError err = new FailingEndpointAlertableError()
            //{
            //    Id = message.Id.ToString(),
            //    Message = "Exception 100: " + message.Id
            //};
            //await context.Publish(err).ConfigureAwait(false);
            //try
            //{
            //    if (!Program.FailedMessages.ContainsKey(context.MessageId))
            //    {
            //        Program.FailedMessages.Add(context.MessageId, "Exception_100");
            //        Console.WriteLine($"Exception logged for {context.MessageId}.");
            //        IAmAnAlertableError err = new FailingEndpointAlertableError()
            //        {
            //            Id = message.Id.ToString(),
            //            Message = "Exception 100: " + message.Id
            //        };
            //        await context.Publish(err).ConfigureAwait(true);
            //    }
            //    throw new Exception("I'm failing");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}
        }
    }
}
